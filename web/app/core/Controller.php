<?php

namespace app\core;

class Controller extends \core\Controller {

	protected $_messageStatus;

	public function __construct()
	{
		\Holiday::$app->language->loadLanguage('Content');
		
		$this->_messageStatus = new \MessageStatus;
		$this->view()->addVars(['messageStatus'=>$this->_messageStatus]);

		$userid = intval(\Holiday::$app->session->get('loggedUser'));
		$this->loggedUser = \app\models\MemberModel::findUnique([['where','id','=',($userid ?? 0)]]);
		$this->view()->addVars(['loggedUser'=>$this->loggedUser]);
	}

	public function showPosts(int $author = 0)
	{
		$postModel = new \app\models\PostModel;

		$posts_per_page = configItem('post.posts_per_page');
		$number_of_posts = intval($postModel->count());
		$current_pages = $_GET['page'] ?? 1;
		$totalPage = max(1, $posts_per_page > 0 ? ceil($number_of_posts / $posts_per_page) : 0);

		$post_offset = $posts_per_page * ($current_pages - 1);
		$post_limit = $posts_per_page > 0 ? $posts_per_page : 1;

		if ($current_pages > $totalPage)
		{
			\Holiday::showError(lang('Content','page_not_found'));
			return;
		}

		$pagination = [
			'pageNow'	=> $current_pages,
			'totalPage'	=> $totalPage
		];
		
		$criteria = [];
		if ($author > 0)
			$criteria[] = ['where','author_id','=',$author];

		$posts = \app\models\PostModel::findByCriteria($criteria,$post_limit,$post_offset,['field'=>'made','order'=>'DESC',]);
		$this->view()->render('main',['posts'=>$posts,'pagination'=>$pagination,'author'=>$author]);
	}

}