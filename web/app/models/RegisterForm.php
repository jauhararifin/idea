<?php

namespace app\models;

class RegisterForm extends \core\model\FormModel {

	public function __construct()
	{
		foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Content', $key);

		parent::__construct();
	}

	public $className = __CLASS__;

	public $attributes = [
		'username' => [
			'label' => "Username",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',32],
	        	['UniqueInTable','member','username'],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'fullname' => [
			'label' => "Fullname",
			'validations' => [
	        	['Required'],
	        	['MaxLen',100],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'email' => [
			'label' => "Email",
			'validations' => [
	        	['Required'],
	        	['MaxLen',255],
	        	['ValidEmail'],
	        	['UniqueInTable','member','email'],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'password' => [
	  		'label' => "Password",
	    	'validations' => [
				['Required'],
				['MaxLen', 32],
			],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'cpassword' => [
	  		'label' => "Confirmation password",
	    	'validations' => [
				['Required'],
				['MaxLen', 32],
				['Equals', 'password'],
			],
			'sanitations' => [
				'Htmlencode',
			],
		],
	];

}
