<?php

namespace app\models;

class StoryForm extends \core\model\FormModel {

	public function __construct()
	{
		parent::__construct();
		foreach ($this->attributes as $key => $value)
			$this->attributes[$key]['label'] = lang('Content', $key);
	}

	public $className = __CLASS__;

	public $attributes = [
		'title' => [
			'label' => "Title",
	    	'validations' => [
	        	['Required'],
	        	['MaxLen',100],
	    	],
			'sanitations' => [
				'Htmlencode',
			],
		],
		'content' => [
	  		'label' => "Content",
	    	'validations' => [
				['Required'],
			],
			'sanitations' => [
				'XssClean',
			],
		]
	];

}
