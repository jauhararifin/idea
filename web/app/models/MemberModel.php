<?php

namespace app\models;

class MemberModel extends \core\model\ActiveRecord {

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		foreach (self::$columns as $key => $value)
			self::$columns[$key]['label'] = lang('Content', $key);
	}

	public static $className = __CLASS__;

	public static $tableName = 'member';

	public static $relations = [
		'post' => [
			'type'=>\core\model\ActiveRecord::HAS_MANY,
			'domesticKey' => 'id',
			'foreignClass' => '\\app\\models\\PostModel',
			'foreignKey' => 'author_id',
		],
	];

	public static $primaryKey = 'id';

	public static $columns = [
		'id' => [
			'label' => "ID",
		],
		'username' => [
			'label' => "Username",
		],
		'email' => [
			'label' => "Email",
		],
		'password' => [
			'label' => "Password",
		],
		'registered' => [
			'label' => "Created",
		],
		'fullname' => [
			'label' => "Full name",
		],
		'data' => [
			'label' => "Data",
		],
	];

}
