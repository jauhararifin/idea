<?php

namespace app\models;

class PostModel extends \core\model\ActiveRecord {

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
		foreach (self::$columns as $key => $value)
			self::$columns[$key]['label'] = lang('Content', $key);
	}

	public static $className = __CLASS__;

	public static $tableName = 'post';

	public static $relations = [
		'author' => [
			'type'=>\core\model\ActiveRecord::HAS_ONE,
			'domesticKey' => 'author_id',
			'foreignClass' => '\\app\\models\\MemberModel',
			'foreignKey' => 'id'
		],
	];

	public static $primaryKey = 'id';

	public static $columns = [
		'id' => array(
			'label' => "ID",
		),
		'title' => array(
			'label' => "Title",
		),
		'author_id' => array(
			'label' => "Authod ID",
		),
		'made' => array(
			'label' => "made",
		),
		'content' => array(
			'label' => "Content",
		),
		'data' => array(
			'label' => "Data",
		),
	];

	public function dateMade($format='Y-m-d H:i:s') {
		return date($format,strtotime($this->made));
	}

}
