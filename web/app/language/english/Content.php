<?php

return array(

	'page_not_found'		=> "Page not found",

	'id'					=> "ID",
	'title'					=> "Title",
	'author'				=> "Author",
	'author_id'				=> "Author ID",
	'last_modified'			=> "Last modified",
	'content'				=> "Content",
	'status'				=> "Status",
	'fullname'				=> "Fullname",
	'email'					=> "Email",

	'username'				=> "Username",
	'password'				=> "Password",
	'data'					=> "Data",

);