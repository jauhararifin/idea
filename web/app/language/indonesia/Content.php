<?php

return array(

	'page_not_found'		=> "Halaman tidak ditemukan.",

	'id'					=> "ID",
	'title'					=> "Judul",
	'author'				=> "Penulis",
	'author_id'				=> "ID Author",
	'last_modified'			=> "Terakhir diperbarui",
	'content'				=> "Isi",
	'status'				=> "Status",
	'fullname'				=> "Nama lengkap",
	'email'					=> "Email",
	'made'					=> "Tanggal pembuatan",
	'username'				=> "Username",
	'password'				=> "Kata sandi",
	'cpassword'				=> "Konfirmasi kata sandi",
	'confirmation_password'	=> "Konfirmasi kata sandi",
	'data'					=> "Data",
	'registered'			=> "Tanggal mendaftar",

	'login'					=> "Masuk",
	'logout'				=> "Keluar",
	'register'				=> "Daftar",
	'create'				=> "Buat",
	'create_story'			=> "Buat cerita",
	'my_story'				=> "Ceritaku",

	'login_success'				=> "Login sukses",
	'login_data_invalid'		=> "Data login tidak valid",
	'wrong_username_password'	=> "Username atau password salah",

	'logout_success'			=> "Logout sukses",
	'createStory_success'		=> "Sukses membuat cerita",
	'createStory_error'			=> "Gagal membuat cerita",
	'createStory_data_invalid'	=> "Data tidak valid",
	'deleteStory_success'		=> "Sukses menghapus cerita",
	'deleteStory_error'			=> "Gagal menghapus cerita",
	'deleteStory_denied'		=> "Anda tidak memiliki hak untuk menghapus cerita ini",

	'register_success'			=> "Sukses mendaftar",
	'register_error'			=> "Gagal mendaftar",
	'register_data_invalid'		=> "Data tidak valid",
	'logout_success'			=> "Berhasil logout",

	'page_not_found'			=> "Halaman tidak ditemukan",
	'no_story'					=> "Tidak ada cerita",

);