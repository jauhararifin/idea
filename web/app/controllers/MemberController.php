<?php

namespace app\controllers;

class MemberController extends \app\core\Controller {

	public function actionLogin()
	{
		if (($this->loggedUser->id ?? 0) > 0)
			redirect(baseUrl());

		$loginForm = new \app\models\LoginForm;

		if (isset($_POST['action']) && $_POST['action'] === 'login')
		{
			$username = $_POST['username'] ?? '';
			$password = $_POST['password'] ?? '';

			$loginForm->fillData(['username'=>$username,'password'=>$password]);

			if ($loginForm->runValidation())
			{
				$user = \app\models\MemberModel::findUnique([['where','username','=',$username]]);
				if ($user != NULL && password_verify($password, $user->password))
				{
					$userSession = [];
					foreach (['id','username','fullname','email','registered'] as $key)
						$userSession[$key] = $user->$key ?? '';
					
					\Holiday::$app->session->set('loggedUser', $userSession['id'] ?? 0);
					
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "login", lang('Content','login_success'));
					redirect(baseUrl());
				} else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "login", lang('Content','wrong_username_password'));
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "login", lang('Content','login_data_invalid'));
		}

		$this->view()->render('login',['loginForm'=>$loginForm]);
	}

	public function actionRegister()
	{
		if (($this->loggedUser->id ?? 0) > 0)
			redirect(baseUrl());

		$registerForm = new \app\models\RegisterForm;

		if (($_POST['action'] ?? '') === 'register')
		{
			$registerForm->fillData($_POST);

			if ($registerForm->runValidation())
			{
				$user = new \app\models\MemberModel;
				$user->username = $registerForm->getData('username');
				$user->fullname = $registerForm->getData('fullname');
				$user->email = $registerForm->getData('email');
				$user->password = password_hash($registerForm->getData('password'), PASSWORD_BCRYPT);
				$user->registered = date("Y-m-d H:i:s");

				if ($user->insert())
				{
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "register", lang('Content','register_success'));
					redirect(baseUrl().'member/login');
				} else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "register", lang('Content','register_error'));	
			} else
				$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "register", lang('Content','register_data_invalid'));
		}

		$this->view()->render('register',['registerForm'=>$registerForm]);
	}

	public function actionLogout()
	{
		if (($_POST['action'] ?? '') == 'logout')
		{
			if (($this->loggedUser->id ?? 0) <= 0)
			{
				redirect(baseUrl());
				exit;
			}

			\Holiday::$app->session->set('loggedUser',0, TRUE);
		
			$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "logout", lang('Content','logout_success'));
		}
		redirect(baseUrl());
	}

}