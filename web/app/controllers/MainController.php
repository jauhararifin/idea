<?php

namespace app\controllers;

class MainController extends \app\core\Controller {

	public function __construct()
	{
		parent::__construct();
		\Holiday::$app->language->loadLanguage('Content');
	}

	public function actionIndex()
	{
		$this->showPosts();
	}

	public function actionMy()
	{
		if (is_object($this->loggedUser) && $this->loggedUser->id > 0)
			$this->showPosts($this->loggedUser->id);
		else
			redirect(baseUrl());
	}

}
