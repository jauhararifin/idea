<?php

namespace app\controllers;

class StoryController extends \app\core\Controller {

	public function actionCreate()
	{
		if (($this->loggedUser->id ?? 0) > 0)
		{
			$storyForm = new \app\models\StoryForm;

			if (($_POST['action'] ?? '') === 'createStory')
			{
				$storyForm->fillData($_POST);

				if ($storyForm->runValidation())
				{
					$story = new \app\models\PostModel;
					$story->title = $storyForm->getData('title');
					$story->content = $storyForm->getData('content');
					$story->author_id = $this->loggedUser->id;
					$story->made = date("Y-m-d H:i:s");

					if ($story->insert())
					{
						$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "createStory", lang('Content','createStory_success'));
						redirect(baseUrl());
					} else
						$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "createStory", lang('Content','createStory_error'));	
				} else
					$this->_messageStatus->addMessage(\messageStatus::MESSAGE_ERROR, "createStory", lang('Content','createStory_data_invalid'));
			}

			$this->view()->render('createStory',['storyForm'=>$storyForm]);
		} else
			redirect(baseUrl());
	}

	public function actionDelete()
	{
		if (($_POST['action'] ?? '') == 'delete' && ($_POST['id'] ?? 0) > 0)
		{
			$story = \app\models\PostModel::findUnique([['where','id','=',$_POST['id']]]);

			if (($this->loggedUser->id ?? 0) == $story->author->id)
			{
				if ($story !== NULL && !empty($story) && $story->delete())
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_SUCCESS, "deleteStory", lang('Content','deleteStory_success'));
				else 
					$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_ERROR, "deleteStory", lang('Content','deleteStory_error'));
			} else
				$this->_messageStatus->addFlashMessage(\messageStatus::MESSAGE_ERROR, "deleteStory", lang('Content','deleteStory_denied'));

		}
		redirect(baseUrl());
	}

}