<?php $this->render('header', array('title'=>configItem('siteName'))); ?>

<script src='https://code.responsivevoice.org/responsivevoice.js'></script>

<div class="container">
	<div class="row">
	    <div class="col-md-12">
	    	<div style="margin-bottom: 40px;" align="center">
	    		<?php if ($author > 0): ?>
		    	<h1><?php htmlLang('Content','my_story'); ?></h1>
		    	<?php endif; ?>
		    </div>
	    	<?php if (count($posts) > 0): ?>
		    	<?php foreach ($posts as $post): ?>
		    	<div class="panel panel-default">
					<div class="panel-heading">
						<?php html($post->title); ?>
						<?php if (is_object($loggedUser) && ($loggedUser->id == $post->author->id)): ?>
						<div class="pull-right">
							<form class="form-horizontal" action="<?php echo baseUrl().'story/delete'; ?>" method="post">
		                        <input type="hidden" name="action" value="delete">
		                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
		                        <input type="hidden" name="id" value="<?php echo $post->id; ?>">
								<button type="submit" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
							</form>
						</div>
						<?php endif; ?>
					</div>
					<div class="panel-body">
						<ul>
							<li><?php htmlLang('Content','author'); ?> : <?php html($post->author->fullname); ?>
							<li><?php htmlLang('Content','made'); ?> : <?php html($post->dateMade('d F Y H:i:s')); ?>
						</ul>
						<div class="story-content">
							<?php echo $post->content; ?>
						</div>
						<div>
							<select class="story-language"></select>
							<button class="btn btn-default read-story"><span class="glyphicon glyphicon-play"></span></button>
							<button class="btn btn-default stop-story" disabled><span class="glyphicon glyphicon-stop"></span></button>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			<?php else: ?>
			<h1 align="center"><?php htmlLang('Content', 'no_story'); ?></h1>
			<?php endif; ?>
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<nav>
				<ul class="pager">
					<?php if ($pagination['pageNow'] < $pagination['totalPage']): ?>
					<li><a href="<?php echo baseUrl().'?page='.((string)($pagination['pageNow']+1)); ?>"><span aria-hidden="true">&larr;</span> Older</a></li>
					<?php endif; ?>
					<?php if ($pagination['pageNow'] > 1): ?>
					<li><a href="<?php echo baseUrl().'?page='.((string)($pagination['pageNow']-1)); ?>">Newer <span aria-hidden="true">&rarr;</span></a></li>
					<?php endif; ?>
				</ul>
			</nav>
		</div>
	</div>
</div>

<script src="<?php echo $this->asset('js/encoder.js'); ?>"></script>
<script>
	Encoder.EncodeType = "entity";
	function removeTag(str) {
		var state = 0;
		var output = "";
		for (var i = 0; i < str.length; i++) {
			if (str[i] == "<")
				state = 1;
			else if (state == 1 && str[i] == "\"")
				state = 2;
			else if (state == 2 && str[i] == "\"")
				state = 1;
			else if (state == 1 && str[i] == ">")
				state = 0;
			else if (!state)
				output += str[i];
		}
		return output;
	}
	$(document).ready(function(){
		var lang = ["UK English Female",
					"UK English Male",
					"US English Female",
					"Arabic Male",
					"Armenian Male",
					"Australian Female",
					"Brazilian Portuguese Female",
					"Chinese Female",
					"Czech Female",
					"Danish Female",
					"Deutsch Female",
					"Dutch Female",
					"Finnish Female",
					"French Female",
					"Greek Female",
					"Hatian Creole Female",
					"Hindi Female",
					"Hungarian Female",
					"Indonesian Female",
					"Italian Female",
					"Japanese Female",
					"Korean Female",
					"Latin Female",
					"Norwegian Female",
					"Polish Female",
					"US English Male"];
		var str = "";
		for (var i = 0; i < lang.length; i++)
			str += "<option value=\""+lang[i]+"\" " + (lang[i] == "Indonesian Female" ? "selected" : "") + ">" + lang[i] + "</option>";
		$(".story-language").append(str);

		$("button.read-story").click(function(){
			var button = $(this);
			var story = button.parent().siblings(".story-content").html();
			story = removeTag(story);
			story = Encoder.htmlDecode(story);
			var language = button.siblings(".story-language").val();
			console.log(story);
			console.log(language);
			responsiveVoice.speak(story, language,{
				onstart: function() {
					console.log(button);
					button.siblings(".stop-story").removeAttr("disabled");
				},
				onend: function() {
					button.siblings(".stop-story").attr("disabled","");
				}
			});
		});
		
		$("button.stop-story").click(function(){
			responsiveVoice.cancel();
			$(this).attr("disabled","");
		})
	});
</script>

<?php $this->render('footer'); ?>