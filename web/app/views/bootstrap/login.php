<?php $this->render('header', array('title'=>configItem('siteName'))); ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php htmlLang('Content','login'); ?></h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'member/login'; ?>" method="post">
                        <input type="hidden" name="action" value="login">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php htmlLang('Content','username'); ?></label>
                            <div class="col-sm-9">
                                <input name="username" type="text" class="form-control" placeholder="<?php htmlLang('Content','username'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php htmlLang('Content','password'); ?></label>
                            <div class="col-sm-9">
                                <input name="password" type="password" class="form-control" placeholder="<?php htmlLang('Content','password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','login'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->render('footer'); ?>