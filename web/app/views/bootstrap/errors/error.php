<!DOCTYPE html>
<html>
    <head>
        <title><?php html($title); ?></title>

        <link rel="stylesheet" type="text/css" href="<?php echo $this->asset('css/bootstrap.min.css'); ?>">
        <script src="<?php echo $this->asset('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo $this->asset('js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
    	<div class="container" style="margin-top: 60px;">
    		<div class="col-md-offset-2 col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Something Error <?php html($statusCode); ?></h4>
					</div>
					<div class="panel-body">
						<?php html($message); ?>
					</div>
				</div>
			</div>
    	</div>
   	</body>
</html>
