<?php $this->render('header', array('title'=>configItem('siteName'))); ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php htmlLang('Content','register'); ?></h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'member/register'; ?>" method="post">
                        <input type="hidden" name="action" value="register">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        
                        <?php $err = $registerForm->validationError('username'); ?>
                        <div class="form-group <?php echo (count($err) > 0 ? "has-error" : ""); ?>">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','username'); ?></label>
                            <div class="col-sm-8">
                                <input name="username" type="text" class="form-control" placeholder="<?php htmlLang('Content','username'); ?>" value="<?php html($registerForm->getData('username')); ?>">
                                <?php if (count($err) > 0): ?>
                                <span class="help-block"><?php html($err[0]); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <?php $err = $registerForm->validationError('fullname'); ?>
                        <div class="form-group <?php echo (count($err) > 0 ? "has-error" : ""); ?>">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','fullname'); ?></label>
                            <div class="col-sm-8">
                                <input name="fullname" type="text" class="form-control" placeholder="<?php htmlLang('Content','fullname'); ?>" value="<?php html($registerForm->getData('fullname')); ?>">
                                <?php if (count($err) > 0): ?>
                                <span class="help-block"><?php html($err[0]); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <?php $err = $registerForm->validationError('email'); ?>
                        <div class="form-group <?php echo (count($err) > 0 ? "has-error" : ""); ?>">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','email'); ?></label>
                            <div class="col-sm-8">
                                <input name="email" type="email" class="form-control" placeholder="<?php htmlLang('Content','email'); ?>" value="<?php html($registerForm->getData('email')); ?>">
                                <?php if (count($err) > 0): ?>
                                <span class="help-block"><?php html($err[0]); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <?php $err = $registerForm->validationError('password'); ?>
                        <div class="form-group <?php echo (count($err) > 0 ? "has-error" : ""); ?>">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','password'); ?></label>
                            <div class="col-sm-8">
                                <input name="password" type="password" class="form-control" placeholder="<?php htmlLang('Content','password'); ?>">
                                <?php if (count($err) > 0): ?>
                                <span class="help-block"><?php html($err[0]); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <?php $err = $registerForm->validationError('cpassword'); ?>
                        <div class="form-group <?php echo (count($err) > 0 ? "has-error" : ""); ?>">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','confirmation_password'); ?></label>
                            <div class="col-sm-8">
                                <input name="cpassword" type="password" class="form-control" placeholder="<?php htmlLang('Content','confirmation_password'); ?>">
                                <?php if (count($err) > 0): ?>
                                <span class="help-block"><?php html($err[0]); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','register'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->render('footer'); ?>