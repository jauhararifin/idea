<?php $this->render('header', array('title'=>configItem('siteName'))); ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <link rel="stylesheet" type="text/css" href="<?php echo $this->asset('js/trumbowyg/ui/trumbowyg.min.css'); ?>">
            <script src="<?php echo $this->asset('js/trumbowyg/trumbowyg.min.js'); ?>"></script>
            <script>
                $(document).ready(function(){
                    $('textarea[name=content]').trumbowyg({
                        fullscreenable: false,
                        closable: false,
                        btns: ['viewHTML','|','bold', 'italic', 'underline','strikethrough','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull','unorderedList', 'orderedList']
                    });
                });
            </script>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php htmlLang('Content','create_story'); ?></h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'story/create'; ?>" method="post">
                        <input type="hidden" name="action" value="createStory">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        
                        <?php $err = $storyForm->validationError('username'); ?>
                        <div class="form-group <?php echo (count($err) > 0 ? "has-error" : ""); ?>">
                            <label class="col-sm-2 control-label"><?php htmlLang('Content','title'); ?></label>
                            <div class="col-sm-10">
                                <input name="title" type="text" class="form-control" placeholder="<?php htmlLang('Content','title'); ?>" value="<?php html($storyForm->getData('username')); ?>">
                                <?php if (count($err) > 0): ?>
                                <span class="help-block"><?php html($err[0]); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php htmlLang('Content','content'); ?></label>
                            <div class="col-sm-10">
                                <textarea name="content" placeholder="<?php htmlLang('Content','content'); ?>"><?php html($storyForm->getData('content')); ?></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','create'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->render('footer'); ?>