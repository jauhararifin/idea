<!DOCTYPE html>
<html>
    <head>
        <title><?php html($title) ?? ''; ?></title>

        <link rel="stylesheet" type="text/css" href="<?php echo $this->asset('css/bootstrap.min.css'); ?>">
        <script src="<?php echo $this->asset('js/jquery.min.js'); ?>"></script>
        <script src="<?php echo $this->asset('js/bootstrap.min.js'); ?>"></script>

        <style>
            .panel {
                border-radius: 0;
            }
            .panel .panel-heading {
                background-color: #337ab7;
                color: white;
            }
        </style>
    </head>
    <body>

    <form id="logout-frm" method="post" action="<?php echo baseUrl().'member/logout'; ?>">
        <input type="hidden" name="action" value="logout">
        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
    </form>

    <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo baseUrl(); ?>"><?php html(configItem('siteName')); ?></a>
            </div>
            <div class="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php if (is_object($loggedUser) && ($loggedUser->id > 0)): ?>
                    <li><a href="<?php echo baseUrl().'mystory'; ?>"><?php htmlLang('Content', 'my_story'); ?></a></li>
                    <li><a data-toggle="modal" href="#story-modal"><?php htmlLang('Content', 'create_story'); ?></a></li>
                    <li><a href="#" onclick="$('#logout-frm').submit();"><?php htmlLang('Content', 'logout'); ?>(<?php html($loggedUser->fullname); ?>)</a></li>
                    <?php else: ?>
                    <li><a data-toggle="modal" href="#login-modal"><?php htmlLang('Content', 'login'); ?></a></li>
                    <li><a data-toggle="modal" href="#register-modal"><?php htmlLang('Content', 'register'); ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

    <div id="login-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="panel-title"><?php htmlLang('Content','login'); ?></h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'member/login'; ?>" method="post">
                        <input type="hidden" name="action" value="login">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php htmlLang('Content','username'); ?></label>
                            <div class="col-sm-9">
                                <input name="username" type="text" class="form-control" placeholder="<?php htmlLang('Content','username'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php htmlLang('Content','password'); ?></label>
                            <div class="col-sm-9">
                                <input name="password" type="password" class="form-control" placeholder="<?php htmlLang('Content','password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','login'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="register-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="panel-title"><?php htmlLang('Content','register'); ?></h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'member/register'; ?>" method="post">
                        <input type="hidden" name="action" value="register">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','username'); ?></label>
                            <div class="col-sm-8">
                                <input name="username" type="text" class="form-control" placeholder="<?php htmlLang('Content','username'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','fullname'); ?></label>
                            <div class="col-sm-8">
                                <input name="fullname" type="text" class="form-control" placeholder="<?php htmlLang('Content','fullname'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','email'); ?></label>
                            <div class="col-sm-8">
                                <input name="email" type="email" class="form-control" placeholder="<?php htmlLang('Content','email'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','password'); ?></label>
                            <div class="col-sm-8">
                                <input name="password" type="password" class="form-control" placeholder="<?php htmlLang('Content','password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?php htmlLang('Content','confirmation_password'); ?></label>
                            <div class="col-sm-8">
                                <input name="cpassword" type="password" class="form-control" placeholder="<?php htmlLang('Content','confirmation_password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','register'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php if (is_object($loggedUser) && $loggedUser->id > 0) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->asset('js/trumbowyg/ui/trumbowyg.min.css'); ?>">
    <script src="<?php echo $this->asset('js/trumbowyg/trumbowyg.min.js'); ?>"></script>
    <script>
        $(document).ready(function(){
            $('textarea[name=content]').trumbowyg({
                fullscreenable: false,
                closable: false,
                btns: ['viewHTML','|','bold', 'italic', 'underline','strikethrough','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull','unorderedList', 'orderedList']
            });
        });
    </script>
    <div id="story-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="panel-title"><?php htmlLang('Content','create_story'); ?></h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'story/create'; ?>" method="post">
                        <input type="hidden" name="action" value="createStory">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php htmlLang('Content','title'); ?></label>
                            <div class="col-sm-10">
                                <input name="title" type="text" class="form-control" placeholder="<?php htmlLang('Content','title'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php htmlLang('Content','content'); ?></label>
                            <div class="col-sm-10">
                                <textarea name="content" placeholder="<?php htmlLang('Content','content'); ?>"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','create'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>