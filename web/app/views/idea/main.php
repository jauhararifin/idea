<!DOCTYPE html>
<html>
<head>
	<title>Story - Bagikan ceritamu ke semua!</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>

	
	<link href="<?php echo $this->asset('global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>	
	<link href="<?php echo $this->asset('/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->asset('/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css"/>

	<link href="<?php echo $this->asset('/global/css/components.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->asset('/global/css/plugins.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->asset('/admin/layout/css/layout.css'); ?>" rel="stylesheet" type="text/css"/>
	<link id="style_color" href="<?php echo $this->asset('/admin/layout/css/themes/darkblue.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->asset('/admin/layout/css/custom.css'); ?>" rel="stylesheet" type="text/css"/>
	
	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="page-sidebar-hide">

	<form id="logout-frm" method="post" action="<?php echo baseUrl().'member/logout'; ?>">
        <input type="hidden" name="action" value="logout">
        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
    </form>

    <div class="navbar navbar-default" style="margin-bottom: 0px;margin-right: 100px;margin-left: 100px">
        <div class="container">
            <div class="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                	<?php if (is_object($loggedUser) && ($loggedUser->id > 0)): ?>
                    <li><a href="<?php echo baseUrl().'mystory'; ?>"><?php htmlLang('Content', 'my_story'); ?></a></li>
                    <!-- <li><a data-toggle="modal" href="#story-modal"><?php htmlLang('Content', 'create_story'); ?></a></li> -->
                    <li><a href="#" onclick="$('#logout-frm').submit();"><?php htmlLang('Content', 'logout'); ?>(<?php html($loggedUser->fullname); ?>)</a></li>
                    <?php else: ?>
                    <li><a data-toggle="modal" href="#login-modal"><?php htmlLang('Content', 'login'); ?></a></li>
                    <li><a data-toggle="modal" href="#register-modal"><?php htmlLang('Content', 'register'); ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

	<div id="login-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="panel-title"><?php htmlLang('Content','login'); ?></h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?php echo baseUrl().'member/login'; ?>" method="post">
                        <input type="hidden" name="action" value="login">
                        <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php htmlLang('Content','username'); ?></label>
                            <div class="col-sm-9">
                                <input name="username" type="text" class="form-control" placeholder="<?php htmlLang('Content','username'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php htmlLang('Content','password'); ?></label>
                            <div class="col-sm-9">
                                <input name="password" type="password" class="form-control" placeholder="<?php htmlLang('Content','password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-default"><?php htmlLang('Content','login'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

	<div class="modal fade draggable-modal" id="register-modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Daftar Akun Baru</h4>
				</div>
				<form class="form-horizontal" role="form" method="post" action="<?php echo baseUrl().'member/register'; ?>">
				<input type="hidden" name="action" value="register">
                <input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
				<div class="modal-body">
									
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label">Username</label>
							<div class="col-md-9">
								<input type="text" name="username" class="form-control" placeholder="username">
							</div>
							<br>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Nama Lengkap</label>
							<div class="col-md-9">
								<input type="text" name="fullname" class="form-control" placeholder="Nama Lengkap">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Email</label>
							<div class="col-md-9">
								<input type="text" name="email" class="form-control" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Kata Sandi</label>
							<div class="col-md-9">
								<input type="password" name="password" class="form-control" placeholder="Kata Sandi">
							</div>
						</div>		
						<div class="form-group">
							<label class="col-md-3 control-label">Konfirmasi Kata Sandi</label>
							<div class="col-md-9">
								<input type="password" name="cpassword" class="form-control" placeholder="Konfirmasi Kata Sandi">
							</div>
						</div>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn blue">Daftar</button>
				</div>
				</form>
			</div>
		</div>
	</div>	

	<div class="page-container">
		
		<div class="page-content-wrapper">
			<div class="page-content">

				<a href="<?php echo baseUrl(); ?>"><h3 class="page-title text-center">
				Story Board</a><br><small class="font-blue-steel">Bagikan ceritamu ke semua orang!</small>
				</h3>
				<br>
				<br>
						

						<div class="note note-success">
							<p>
								 Hai, kami Fadhil Imam Kurnia, Jauhar Arifin, dan M Hilmi Asyrofi dari Kelompok 2 Tubes ARC. Kami membuat website <b>Story Board</b> ini sebagai wadah bagi kalian CaKru ARC untuk saling berbagi cerita yang kalian miliki. 
								 Berbagai macam ceriita dapat kalian <i>share</i> di sini, baik tentang teknologi, tentang keakraban, dan sebagainya. So, <b>ayo bagikan cerita-cerita menarik kalian di sini! </b>
							</p>
							<?php if (is_object($loggedUser) && ($loggedUser->id > 0)): ?>
							<p>
								Mulai share ceritamu dengan klik tombol "<b>Tulis Cerita</b>" di samping ini
							</p>
							<p class="text-right">
								<a class="btn blue" data-toggle="modal" href="#buat_cerita"><i class="fa fa-pencil"></i> Tulis Cerita </a>
							</p>
							<?php endif; ?>
						</div>

						<div class="modal fade" id="buat_cerita" tabindex="-1" role="basic" aria-hidden="true" style="overflow-y:scroll;">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Bagikan ceritamu disini</h4>
									</div>
									<form class="form-horizontal" role="form" method="post" action="<?php echo baseUrl().'story/create'; ?>">
									<div class="modal-body">										
										<input type="hidden" name="action" value="createStory">
		                    			<input type="hidden" name="<?php echo csrfTest(); ?>" value="<?php echo csrfToken(); ?>">
										<div class="form-body">
											<div class="form-group">
												<label class="col-md-3 control-label">Judul Cerita</label>
												<div class="col-md-9">
													<input name="title" type="text" name="judul" class="form-control" placeholder="Judul cerita kamu ...">
												</div>
											</div>
											<textarea row="1" name="content"></textarea>										
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn default" data-dismiss="modal">Tutup</button>
										<button type="submit" class="btn blue">Bagikan</button>
									</div>
									</form>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>						
				
				<div class="row">
					<div class="col-md-4">
						Halaman:
					</div>
					<div class="col-md-4 text-center">
						<div class="btn-group">
							<?php if ($pagination['pageNow'] > 1): ?>
							<a href="<?php echo baseUrl().'?page='.((string)($pagination['pageNow']-1)); ?>">
								<button type="button" class="btn btn-default"><span class="fa fa-arrow-left"></span> Sebelum</button>
							</a>
							<?php endif; ?>

							<?php if ($pagination['pageNow'] < $pagination['totalPage']): ?>
							<a href="<?php echo baseUrl().'?page='.((string)($pagination['pageNow']+1)); ?>">
								<button type="button" class="btn btn-default">Sesudah <span class="fa fa-arrow-right"></span></button>
							</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-4">
						&nbsp;
					</div>
				</div>
				<hr>
				<div class="row" id="sortable_portlets">
					<?php $data1 = ""; $data2= ""; $data3=""; ?>
					<?php for ($i = 0; $i < count($posts); $i++): ?>
					<?php $post = $posts[$i]; $str = '
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption blue">
								<i class="icon-pin blue"></i>
								<span class="caption-subject bold uppercase">'.($post->title).'</span>
								<br>
								<span class="caption-helper" style="font-size:70%">'.($post->author->fullname).'</span>
							</div>
							<div class="actions">
								'.((is_object($loggedUser) && ($loggedUser->id == $post->author->id)) ? '
								<form class="form-delete" data-id="'.$post->id.'" class="form-horizontal" action="'.baseUrl().'story/delete'.'" method="post">
			                        <input type="hidden" name="action" value="delete">
			                        <input type="hidden" name="'.csrfTest().'" value="'.csrfToken().'">
			                        <input type="hidden" name="id" value="'.$post->id.'">
								</form>
								<a href="#" onclick="$(\'.form-delete[data-id='.$post->id.']\').submit();" class="btn btn-circle btn-default btn-sm red" style="color:white">
								<i style="color:white" class="fa fa-trash"></i> Hapus </a>' : '').'
							</div>
						</div>
						<div class="portlet-body">
							<div id="story-content-'.$post->id.'" class="scroller" style="height:200px;overflow-y:auto" data-rail-visible="1" data-rail-color="blue" data-handle-color="#a1b2bd">
								<p>
									'.$post->content.'
								</p>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-6">
									<button type="button" class="btn blue read-story" data-id='.$post->id.'><i class="fa fa-play"></i></button>
									<button type="button" class="btn stop-story"><i class="fa fa-stop"></i></button>
								</div>
								<div class="col-md-6 text-right">
									<select data-id='.$post->id.' class="form-control story-language">
									</select>
									<small>'.$post->made.'</small>
								</div>									
							</div>
						</div>
					</div>';
					if ($i % 3 == 0) $data1 .= $str;
					else if ($i % 3 == 1) $data2 .= $str;
					else if ($i % 3 == 2) $data3 .= $str; ?>
					<?php endfor; ?>
					<?php echo '<div class="col-md-4 column">
						'.$data1.'
						<div class="portlet portlet-empty">
						</div>
					</div>
					<div class="col-md-4 column">
						'.$data2.'
						<div class="portlet portlet-empty">
						</div>
					</div>
					<div class="col-md-4 column">
						'.$data3.'
						<div class="portlet portlet-empty">
						</div>
					</div>'; ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="page-footer" style="margin-left:100px">
		<div class="page-footer-inner">
			 	2016 &copy; Fadhil Imam Kurnia | Jauhar Arifin | M Hilmi Asyrofi
		</div>
	</div>

	<script src="<?php echo $this->asset('global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
	
	<script src="<?php echo $this->asset('global/plugins/jquery-ui/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>

	<script src="<?php echo $this->asset('global/scripts/metronic.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('admin/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('admin/layout/scripts/quick-sidebar.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo $this->asset('admin/layout/scripts/demo.js'); ?>" type="text/javascript"></script>
	<script>
	jQuery(document).ready(function() {       
	   	Metronic.init();
		Layout.init();
		Demo.init();
	    $("#buat_cerita").draggable({
	    	handle: ".modal-header"
	    });
	</script>
	<script src='https://code.responsivevoice.org/responsivevoice.js'></script>
	<script src="<?php echo $this->asset('js/encoder.js'); ?>"></script>
	<script>
		Encoder.EncodeType = "entity";
		function removeTag(str) {
			var state = 0;
			var output = "";
			for (var i = 0; i < str.length; i++) {
				if (str[i] == "<")
					state = 1;
				else if (state == 1 && str[i] == "\"")
					state = 2;
				else if (state == 2 && str[i] == "\"")
					state = 1;
				else if (state == 1 && str[i] == ">")
					state = 0;
				else if (!state)
					output += str[i];
			}
			return output;
		}
		$(document).ready(function(){
			var lang = ["UK English Female",
						"UK English Male",
						"US English Female",
						"Arabic Male",
						"Armenian Male",
						"Australian Female",
						"Brazilian Portuguese Female",
						"Chinese Female",
						"Czech Female",
						"Danish Female",
						"Deutsch Female",
						"Dutch Female",
						"Finnish Female",
						"French Female",
						"Greek Female",
						"Hatian Creole Female",
						"Hindi Female",
						"Hungarian Female",
						"Indonesian Female",
						"Italian Female",
						"Japanese Female",
						"Korean Female",
						"Latin Female",
						"Norwegian Female",
						"Polish Female",
						"US English Male"];
			var str = "";
			for (var i = 0; i < lang.length; i++)
				str += "<option value=\""+lang[i]+"\" " + (lang[i] == "Indonesian Female" ? "selected" : "") + ">" + lang[i] + "</option>";
			$(".story-language").append(str);

			$("button.read-story").click(function(){
				var button = $(this);
				var id = $(this).attr('data-id');
				var str = "#story-content-"+id;
				var story = $(str).html();
				story = removeTag(story);
				story = Encoder.htmlDecode(story);
				var language = $(".story-language[data-id="+id+"]").val();
				console.log(story);
				console.log(language);
				responsiveVoice.speak(story, language,{
					onstart: function() {
						console.log(button);
						button.siblings(".stop-story").removeAttr("disabled");
					},
					onend: function() {
						button.siblings(".stop-story").attr("disabled","");
					}
				});
			});
			
			$("button.stop-story").click(function(){
				responsiveVoice.cancel();
				$(this).attr("disabled","");
			})
		});
	</script>
	<script type="text/javascript" src="<?php echo $this->asset('js/noty/packaged/jquery.noty.packaged.min.js'); ?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        <?php foreach ($messageStatus->getMessages() as $type => $messages): ?>
        <?php foreach ($messages as $mess): ?>
        noty({
          text  :"<?php html($mess); ?>",
          type  :"<?php echo $type; ?>",
          dismissQueue: true,
          layout:'bottomRight',
        });
        <?php endforeach; ?>
        <?php endforeach; ?>

      });
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->asset('js/trumbowyg/ui/trumbowyg.min.css'); ?>">
    <script src="<?php echo $this->asset('js/trumbowyg/trumbowyg.min.js'); ?>"></script>
    <script>
        $(document).ready(function(){
            $('textarea[name=content]').trumbowyg({
                fullscreenable: false,
                closable: false,
                btns: ['viewHTML','|','bold', 'italic', 'underline','justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull','unorderedList', 'orderedList']
            });
        });
    </script>
</body>
</html>