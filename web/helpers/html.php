<?php

function wordLimit(string $text, int $num, string $ellipsis='...') : string
{
  $inQuote = FALSE;
  $firstQuote = '';
  $inTag = FALSE;
  $count = 0;
  $ellipsised = FALSE;

  $quotes = array("'", "\"", "`");
  $whitespaces = array("\n","\r","\t","\s"," ");

  $output = '';

  for ($i = 0; $i < strlen($text); $i++)
  {
    $c = $text[$i];

    if (! $inTag && $c == '<')
    {
      $inTag = TRUE;
      $output .= $c;
    }else if (! $inQuote && $inTag && $c == '>')
    {
      $inTag = FALSE;
      $output .= $c;
    }else if ($inQuote && $c == '\\')
    {
      $output .= $c;
      $i++;
      if ($i < strlen($text))
        $output .= $text[$i];
    }else if ($inQuote && $c == $firstQuote)
    {
      $output .= $c;
      $inQuote = FALSE;
    }else if ($inTag && ! $inQuote && in_array($c, $quotes))
    {
      $output .= $c;
      $inQuote = TRUE;
    }else if ($inTag)
    {
      $output .= $c;
    }else
    {
      $b = ($i > 0) ? $text[$i-1] : '';
      if (!in_array($c, $whitespaces) && (in_array($b, $whitespaces) || in_array($b, array('<','>'))))
        $count++;
      if ($count <= $num)
        $output .= $c;
      else if ($count == $num+1 && ! $ellipsised)
      {
        $output .= $ellipsis;
        $ellipsised = TRUE;
      }
    }

  }

  return $output;
}

function html($text)
{
  if (is_array($text))
    foreach ($text as $t)
      html($t);
  else
    echo htmlentities($text);
}