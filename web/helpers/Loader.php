<?php

namespace helpers;

class Loader {

	public static function load(string $filename)
	{
		$dir = [
			SYSTEM_PATH.'app/helpers/',
			SYSTEM_PATH.'helpers/',
		];

		foreach ($dir as $directory)
		{
			$file = $directory.$filename.'.php';
			if (is_dir($directory) && file_exists($file))
				require_once $file;
		}
	}

}