<?php

function redirect(string $url)
{
	if ($url != '')
	{
		header('Location: '.$url);
		exit;
	}
}

function baseUrl()
{
	return \Holiday::$app->config->item('baseUrl');
}