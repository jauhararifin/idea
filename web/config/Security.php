<?php

return array(

  'auto_csrf_filter'	=> array('POST'), // POST and GET only
  'csrf_test_name'		=> 'csrf_test',
  'csrf_session_name'	=> 'hl_csrf_test',

);
