<?php

return array(

		'active' => 'default',

		'default' => array(

				'driver'		=> 'mysql',
				'host'			=> 'localhost',
				'database'		=> 'idea',
				'username'		=> 'root',
				'password'		=> '',
				'charset'		=> 'utf8',
				'collation'		=> 'utf8_unicode_ci',
				'prefix'		=> 'id_',
				'options'		=> array(),

			),

	);
