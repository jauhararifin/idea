<?php

return array(

	'unable_find_controller'	=> "Tidak dapat menemukan controller.",
	'unable_create_controller'	=> "Tidak dapat menemukan class controller.",
	'unable_find_action'		=> "Tidak dapat menemukan aksi.",
	'file_not_exists'			=> "File tidak ditemukan, {{file}}.",
	'unable_find_database_configuration'	=> "Tidak dapat menemukan konfigurasi database.",
	'action_not_permitted'		=> "Aksi tidak diijinkan.",
	'something_error'			=> "Terjadi error.",
	'system_under_construction_internal_error'	=> "Sistem dalam pengembangan. Internal server error.",

);