<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<style>
			.error_box {
				margin: 10px;
				padding: 10px;
				border: solid 1px #000;
			}
		</style>
	</head>
	<body>
		<div class="error_box">
			<h1><?php echo $statusCode.' '.$title; ?></h1>
			<p><?php echo $message; ?></p>
		</div>
	</body>
</html>
