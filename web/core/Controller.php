<?php

namespace core;

class Controller {

	private $_view;

	public function __construct()
	{
		$this->_view = new View;
	}

	public function view() : \core\View
	{
		if ($this->_view === NULL)
			$this->_view = new View;
		return $this->_view;
	}

}
