<?php

namespace core;

class Session {

	private $_config = array(
		'session_name'		=> 'sessid',
		'session_location'	=> 'holiday/session',
		'flash_location'	=> 'holiday/flash',
	);

	private $_flash_now = array();

	public function __construct(array $config)
	{
		foreach ($config as $key => $val)
			$this->_config[$key] = $val;

		if (isset($this->_config['session_name']))
			session_name((string) $this->_config['session_name']);

		session_start();

		if (isset($_SESSION[$this->_config['flash_location']]))
			$this->_flash_now = $_SESSION[$this->_config['flash_location']];
		$_SESSION[$this->_config['flash_location']] = array();
	}

	public function get(string $param)
	{
		if (!isset($_SESSION[$this->_config['session_location']]))
			$_SESSION[$this->_config['session_location']] = array();

		$arr = $_SESSION[$this->_config['session_location']];
		foreach (explode('.',$param) as $key)
		{
			if (!isset($arr[$key]))
				return NULL;
			else
				$arr = $arr[$key];
		}
		return $arr;
	}

	public function set(string $param, $val, bool $push = FALSE)
	{
		if (!isset($_SESSION[$this->_config['session_location']]))
			$_SESSION[$this->_config['session_location']] = array();

		$arr = &$_SESSION[$this->_config['session_location']];
		foreach (explode('.', $param) as $key)
		{
			if (!is_array($arr))
				$arr = array();
			if (!isset($arr[$key]))
				$arr[$key] = NULL;
			$arr = &$arr[$key];
		}

		if ($push)
		{
			if (!is_array($arr))
				$arr = [];
			$arr = $val;
		} else
			$arr = $val;
	}

	public function setFlash(string $param, $val, bool $push = FALSE)
	{
		if (!isset($_SESSION[$this->_config['flash_location']]))
			$_SESSION[$this->_config['flash_location']] = array();

		$arr = &$_SESSION[$this->_config['flash_location']];

		foreach (explode('.', $param) as $key)
		{
			if (!is_array($arr))
				$arr = array();
			if (!isset($arr[$key]))
				$arr[$key] = NULL;
			$arr = &$arr[$key];
		}

		if ($push)
		{
			if (!is_array($arr))
				$arr = array();
			$arr[] = $val;
		} else
			$arr = $val;
	}

	public function getFlash(string $param)
	{
		if (!isset($this->_flash_now))
			$this->_flash_now = array();

		$arr = $this->_flash_now;
		foreach (explode('.',$param) as $key)
		{
			if (!isset($arr[$key]))
				return NULL;
			else
				$arr = $arr[$key];
		}
		return $arr;
	}

}
