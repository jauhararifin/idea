<?php

namespace core\model;

class FormModel extends \core\Model {

  private $_attribute = array();
  private $_valdateAttribute = array();

  private $InputValidationLib = NULL;

  public function __construct()
  {
    parent::__construct();
    $this->InputValidationLib = new \InputValidation($this->attributes);
  }

  public function __call(string $method, array $args)
  {
    	if (! is_array($this->attributes))
			return NULL;
		if (isset($this->attributes[$method]))
			return isset($this->_attribute[$method]) ? $this->_attribute[$method] : NULL;

    return call_user_func_array([parent,$method], $args);
  }

  public function fillData($name, $value = NULL)
  {
    if (is_array($name))
    {
      foreach ($name as $key => $val)
        $this->fillData($key, $val);
    }else {
      $this->_attribute[$name] = $value;
    }
  }

  public function resetData() {
    foreach ($this->_attribute as $name => $val)
      $this->_attribute[$name] = NULL;
    $this->InputValidationLib->resetData();
  }

  public function getData(string $par = '')
  {
    if ($par == '')
      return $this->_attribute;
    return $this->_attribute[$par] ?? NULL;
  }

  public function getSanitizedData(string $par = '')
  {
    return $this->InputValidationLib->getSanitizedData($par);
  }

  public function runValidation() : bool
  {
    $attrs = $this->attributes;
    if (!is_array($attrs))
      return FALSE;

    $this->InputValidationLib->setData($this->_attribute);
    $this->InputValidationLib->setRules($this->attributes);

    return $this->InputValidationLib->run();
  }

  public function validationError(string $field = '')
  {
    if (isset($this->InputValidationLib) && $this->InputValidationLib != NULL)
      return $this->InputValidationLib->getError($field);
    return [];
  }

  public function attributeStatus(string $attr) : int
  {
    return $this->InputValidationLib->getAttributeStatus($attr);
  }

}
