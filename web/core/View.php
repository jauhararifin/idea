<?php

namespace core;

class View {

	private $_config;
	private $_vars = array();

	public function __construct()
	{
		$this->_config = configItem('view');
		if (empty($this->_config))
			$this->_config = array();
	}

	public function setConfig(string $key, $value)
	{
		if (!is_array($this->_config))
			$this->_config = array();
		$this->_config[$key] = $value;
	}

	public function getConfig(string $key)
	{
		return $this->_config[$key] ?? NULL;
	}

	public function asset(string $file) : string
	{
		if (! isset($this->_config['assetsUrl']))
			return configItem('baseUrl').((string) $this->_config['theme'] !== '' ? (string) $this->_config['theme'].'/' : '').$file;
		return configItem('baseUrl').$this->_config['assetsUrl'].((string) $this->_config['theme'] !== '' ? (string) $this->_config['theme'].'/' : '').$file;
	}

	public function addVars(array $arr = array())
	{
		foreach ($arr as $key => $val)
			$this->_vars[$key] = $val;
	}

	public function render(string $view, array $vars=array(), bool $return = FALSE)
	{
		extract($vars);
		extract($this->_vars);

		ob_start();

		$dir = isset($this->_config['viewDirectory']) ? $this->_config['viewDirectory'] : '';
		$dir .= (string) $this->_config['theme'] !== '' ? (string) $this->_config['theme'].'/' : '';

		if (file_exists($dir.((string) $view).'.php'))
		{
			include ($dir.((string) $view).'.php');

			$content = ob_get_contents();

			if ($return)
			{
				ob_end_clean();
				return $content;
			}else
				ob_end_flush();
		}else
			\Holiday::showError(lang('Core','system_under_construction_internal_error'));
	}

	public function print(string $str)
	{
		ob_start();
		echo $str;
		ob_end_flush();
	}

}
