<?php

namespace core;

class Security {

	const DEFAULT_CSRF_TEST_NAME = 'csrf_test';

	private $_config = NULL;

	private $_csrf_test_name;
	private $_csrf_token_value;

	public function __construct(array $config=array())
	{
		if (! is_array($config))
			$this->_config = array();
		else
		{
			$this->_config = $config;
			if (isset($config['csrf_test_name']))
				$this->_csrf_test_name = $config['csrf_test_name'];
			else
				$this->_csrf_test_name = self::DEFAULT_CSRF_TEST_NAME;
		}
	}

	public function run()
	{
		$this->getCsrfToken();

		if (is_array($this->_config) && isset($this->_config['auto_csrf_filter']))
		{
			if ($this->_config['auto_csrf_filter'] === 'POST' ||
				(is_array($this->_config['auto_csrf_filter']) && in_array('POST', $this->_config['auto_csrf_filter'])))
				$this->csrfVerify('POST', TRUE);

			if ($this->_config['auto_csrf_filter'] === 'GET' ||
				(is_array($this->_config['auto_csrf_filter']) && in_array('GET', $this->_config['auto_csrf_filter'])))
				$this->csrfVerify('GET', TRUE);
		}
	}

	public function getCsrfTestName() : string
	{
		$name = (string) $this->_csrf_test_name;
		if ($name == '' || $name == NULL || $name === NULL)
			return self::DEFAULT_CSRF_TEST_NAME;
		return $name;
	}

	public function getCsrfToken() : string
	{
		if (\Holiday::$app->session->get($this->_csrf_test_name) === NULL)
			return $this->_csrf_token_value = $this->generateCsrfToken();
		else
			return $this->_csrf_token_value = \Holiday::$app->session->get($this->_csrf_test_name);
	}

	public function generateCsrfToken() : string
	{
		$this->_csrf_token_value = md5(time());
		\Holiday::$app->session->set($this->_csrf_test_name, $this->_csrf_token_value);
		return $this->_csrf_token_value;
	}

	public function csrfVerify(string $from='POST', bool $error=FALSE) : bool
	{
		$token = '';
		if (strtoupper($from) === 'POST')
		{
			if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST')
				$token = $_POST[$this->_csrf_test_name] ?? '';
			else
				$token = $this->_csrf_token_value;
		} else  if (strtoupper($from) === 'GET')
		{
			if (strtoupper($_SERVER['REQUEST_METHOD']) == 'GET')
				$token = $_GET[$this->_csrf_test_name] ?? '';
			else
				$token = $this->_csrf_token_value;
		}
		$token = (string) $token;

		if ($token !== $this->_csrf_token_value)
		{
			if ($error)
				\Holiday::showError(lang('Core','action_not_permitted') ,500,'error',lang('Core','something_error'),TRUE);
			return FALSE;
		}
		return TRUE;

	}

}
