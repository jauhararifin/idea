<?php

namespace core;

class Route {

	const CONTROLLER_PATH = 'app/controllers/';
	const CONTROLLER_NAMESPACE = '\\app\\controllers';
	const CONTROLLER_PREFIX = '';
	const CONTROLLER_SUFFIX = 'Controller';
	const ACTION_PREFIX = 'action';
	const ACTION_SUFFIX	= '';
	const ACTION_REMAP_METHOD	= 'action';

	private $_config = NULL;

	private $_directory	= NULL;
	private $_file = NULL;
	private $_class = NULL;
	private $_action = NULL;
	private $_params = array();

	public function __construct(array $config=array())
	{
		$this->_config = $config;
	}

	public function parseRequest()
	{
		$request_string = (string) ($_SERVER['PATH_INFO'] ?? '');
		if ($request_string == '')
			$request_string = $this->_config['defaultRoute'] ?? '';
		if (isset($this->_config['rewrite']) && is_array($this->_config['rewrite']))
			foreach ($this->_config['rewrite'] as $key => $val)
				if (preg_match((string) $key, $request_string))
				{
					$request_string = preg_replace((string) $key, (string) $val, $request_string);
					break;
				}

		$request_array = explode('/', $request_string);
		
		foreach ($request_array as $req)
		{
			if ($req === '')
				continue;

			if ($this->_class === NULL)
			{
				$req = ucfirst($req);
				if (file_exists(SYSTEM_PATH.self::CONTROLLER_PATH.$this->_directory.$req.self::CONTROLLER_SUFFIX.'.php'))
				{
					$this->_file = SYSTEM_PATH.self::CONTROLLER_PATH.$this->_directory.$req.self::CONTROLLER_SUFFIX.'.php';
					$this->_class = self::CONTROLLER_NAMESPACE.'\\'.(str_replace('/', '\\', $this->_directory)).$req.self::CONTROLLER_SUFFIX;
				}else if (is_dir(SYSTEM_PATH.self::CONTROLLER_PATH.$this->_directory.ucfirst($req)))
					$this->_addDirectory(ucfirst($req));
				else
				{
					\Holiday::showError(lang('Core','unable_find_controller'));
					return;
				}
			} else if ($this->_action === NULL)
			{
				$this->_action = $req;
			}
			else if ($req !== NULL)
				$this->_params[] = $req;
		}

		if ($this->_class === NULL)
		{
			\Holiday::showError(lang('Core','unable_find_controller'));
			return;
		}
		
		if ($this->_action === NULL)
			$this->_action = 'index';

		return ($this->_class !== NULL && $this->_action !== NULL);
	}

	private function _normalizePath(string $path) : array
	{
		// menghilangkan double slash, dan slash di awal maupun di akhir
		$path = preg_replace(array('/(\/+)/', '(^\/+|\/+$)'),array('/',''), (string) $path);
		$file = SYSTEM_PATH.self::CONTROLLER_PATH.$path.self::CONTROLLER_SUFFIX.'.php';
		preg_match('(/[^/]+$)', $path, $matches);
		$controller = (count($matches) ? $matches[0] : '');

		return array('path'=>$path, 'file'=>$file, 'controller'=>$controller);
	}

	private function _addDirectory(string $dir = '')
	{
		$this->_directory .= $dir.'/';
	}

	public function __get(string $param)
	{
		if (in_array((string) $param, array ('directory', 'file', 'class', 'action', 'params')))
		{
			$param = '_'.$param;
			return $this->$param;
		}
	}

}
