<?php

class MessageStatus {

	const MESSAGE_INFO		= "information";
	const MESSAGE_WARNING	= "warning";
	const MESSAGE_SUCCESS	= "success";
	const MESSAGE_ERROR		= "error";

	private $_messages = array();

	public function __construct()
	{
		$this->_messages = array(
			self::MESSAGE_ERROR => array(),
			self::MESSAGE_WARNING => array(),
			self::MESSAGE_SUCCESS => array(),
			self::MESSAGE_INFO => array(),
		);

		$flash = \Holiday::$app->session->getFlash('message');
		if (is_array($flash))
			foreach ($flash as $key => $val)
				$this->_messages[$key] = $val;
	}

	public function addMessage(string $type = '', string $identifier = 'default', string $message)
	{
		switch ($type)
		{
			case self::MESSAGE_ERROR:
			case self::MESSAGE_WARNING:
			case self::MESSAGE_SUCCESS:
			case self::MESSAGE_INFO:
				$this->_messages[$type][$identifier][] = $message;
				break;
		}
	}

	public function addFlashMessage(string $type = '', string $identifier = 'default', string $message)
	{
		switch ($type)
		{
			case self::MESSAGE_ERROR:
			case self::MESSAGE_WARNING:
			case self::MESSAGE_SUCCESS:
			case self::MESSAGE_INFO:
				\Holiday::$app->session->setFlash('message.'.$type.'.'.$identifier, $message, TRUE);
				break;
		}
	}

	public function getMessages(string $identifier = '')
	{
		$mess = array();
		foreach ($this->_messages as $type => $messa) {
			$mess[$type] = array();
			if (isset($messa[$identifier]))
				foreach ($messa[$identifier] as $mes)
					$mess[$type][] = $mes;
			else if ($identifier == NULL)
				foreach ($messa as $id)
					foreach ($id as $m)
						$mess[$type][] = $m;
		}
		return $mess;
	}

}
