<?php

class InputValidation {

  const NONE              = 0;
  const VALIDATION_ERROR  = 1;
  const SUCCESS           = 2;

  private $_data          = [];
  private $_dataSanitize  = [];
  private $_errors        = [];
  private $_rules         = [];
  private $_attribute_stat = [];
  private $_attribute_label = [];

  private $_last_error  = "";

  public function __construct($attribute = [])
  {
    $this->_data = [];
    $this->_errors = [];
    $this->_rules = [];

    \Holiday::$app->language->loadLanguage('InputValidation');

    foreach ($attribute as $attr => $val)
    {
      $this->_attribute_label[$attr] = $val['label'] ?? '';
    }
  }

  public function setData($par, $val = NULL)
  {
    if (is_array($par))
      foreach ($par as $key => $val)
      {
        $this->_data[(string) $key] = $val;
        $this->_attribute_stat[(string) $key] = self::NONE;
      }
    else
    {
      $this->_data[(string) $par] = $val;
      $this->_attribute_stat[(string) $key] = self::NONE;
    }
  }

  public function getData(string $par = '')
  {
    if ($par == '')
      return $this->_data;
    return $this->_data[$par] ?? NULL;
  }

  public function getAttributeStatus(string $attr)
  {
    return $this->_attribute_stat[$attr] ?? self::NONE;
  }

  public function resetData()
  {
    $this->_data = array();
    $this->_errors = array();
    foreach ($this->_attribute_stat as $key => $val)
      $this->_attribute_stat[$key] = self::NONE;
  }

  public function getSanitizedData(string $par = '')
  {
    if ($par == '')
      return $this->_dataSanitize;
    return $this->_dataSanitize[$par] ?? NULL;
  }

  public function setRules(array $rules)
  {
    $this->_rules = $rules;
  }

  public function setSanitation(string $field, array $sanitations)
  {
    $this->_rules[$field]['sanitations'] = [];
    foreach ($sanitations as $f)
      $this->_rules[$field]['sanitations'][] = (string) $f;
  }

  public function setValidation(string $field, array $validations)
  {
    $this->_rules[$field]['validations'] = [];
    foreach ($validations as $val)
      $this->_rules[$field]['validations'][] = $val;
  }

  public function run() : bool
  {
    $succ = TRUE;
    foreach ($this->_data as $key => $val)
    {
      $validationRules = array();
      $sanitationRules = array();

      if (isset($this->_rules[$key]))
      {
        if (isset($this->_rules[$key]['validations']))
          $validationRules = $this->_rules[$key]['validations'];
        if (isset($this->_rules[$key]['sanitations']))
          $sanitationRules = $this->_rules[$key]['sanitations'];
      }

      $temp_success = TRUE;
      foreach ($validationRules as $vr)
      {
        $param = array_slice($vr, 1);
        $vn = 'validation'.$vr[0];

        $ret = call_user_func_array(array($this, $vn), array_merge(array($val), $param));
        if (!$ret)
          $this->addError($key, $this->_last_error);

        $temp_success = $temp_success && $ret;
      }
      $this->_attribute_stat[$key] = $temp_success ? self::SUCCESS : self::VALIDATION_ERROR;
      $succ = $succ && $temp_success;

      $this->_dataSanitize[$key] = $val;

      if (is_array($sanitationRules))
        foreach ($sanitationRules as $fr)
        {
          $fn = 'sanitize'.$fr;
          $this->_dataSanitize[$key] = $this->$fn($this->_dataSanitize[$key]);
        }
    }

    return $succ;
  }

  public function addError(string $field, string $message)
  {
    if (!isset($this->_errors[$field]))
      $this->_errors[$field] = [];
    $this->_errors[$field][] = str_replace("{{field}}", $this->_attribute_label[$field], $message);
  }

  public function getError(string $field = '') : array
  {
    if ($field == '')
      return $this->_errors;
    return $this->_errors[$field] ?? [];
  }

  public function sanitizeHtmlencode(string $input, $params = NULL) : string
	{
		return htmlspecialchars($input);
	}

  public function sanitizeXssClean(string $input) : string
  {
    $data = (string) $input;
    
    /*
    * XSS filter 
    *
    * This was built from numerous sources
    * (thanks all, sorry I didn't track to credit you)
    * 
    * It was tested against *most* exploits here: http://ha.ckers.org/xss.html
    * WARNING: Some weren't tested!!!
    * Those include the Actionscript and SSI samples, or any newer than Jan 2011
    *
    *
    * TO-DO: compare to SymphonyCMS filter:
    * https://github.com/symphonycms/xssfilter/blob/master/extension.driver.php
    * (Symphony's is probably faster than my hack)
    */
    // Fix &entity\n;
    $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
    $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

    // Remove any attribute starting with "on" or xmlns
    $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

    // Remove javascript: and vbscript: protocols
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

    // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

    // Remove namespaced elements (we do not need them)
    $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    do
    {
      // Remove really unwanted tags
      $old_data = $data;
      $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
    }
    while ($old_data !== $data);

    // we are done...
    return $data;
  }

  /*================================== Validation ================================== */

  public function validationRequired (string $input) : bool
  {
    if ($input === '' || $input === NULL)
    {
      $this->_last_error = lang('InputValidation','validation_Required');
      return FALSE;
    }
    return TRUE;
  }

  public function validationEquals (string $input, string $field) : bool
  {
    if ($field === '' || $input !== $this->getData($field))
    {
      $this->_last_error = lang('InputValidation','validation_Equals', array('field2'=>$field));
      return FALSE;
    }
    return TRUE;
  }

  public function validationNotEquals (string $input, string $field) : bool
  {
    if ($field === '' || $input === $this->getData($field))
    {
      $this->_last_error = lang('InputValidation','validation_NotEquals', array('field2'=>$field));
      return FALSE;
    }
    return TRUE;
  }

  public function validationContain(string $input, string $words) : bool
  {
    if (!is_array($word))
      $words = array($word);

    $contain = TRUE;
    foreach ($word as $word)
      $contain = $contain && (strpos($input, (string) $word) !== FALSE);

    if (!$contain)
    {
      $this->_last_error = lang('InputValidation','validation_Contain');
      return FALSE;
    }
    return TRUE;
  }

  public function validationNotContain(string $input, string $words) : bool
  {
    if (!is_array($word))
      $words = array($word);

    $contain = FALSE;
    foreach ($words as $word)
      $contain = $contain || (strpos($input, (string) $word) !== FALSE);

    if ($contain)
    {
      $this->_last_error = lang('InputValidation','validation_NotContain');
      return FALSE;
    }
    return TRUE;
  }

  public function validationRegexMatch(string $input, string $pattern) : bool
  {
    if (preg_match($pattern, $input))
      return TRUE;

    $this->_last_error = lang('InputValidation','validation_RegexMatch');
    return FALSE;
  }

  public function validationUnique(string $input, string $table, string $field) : bool
  {
    $model = new \core\Model;
    if ($model->db($table)->where($field,'=',$input)->count() == 0)
      return TRUE;

    $this->_last_error = lang('InputValidation','validation_Unique');
    return FALSE;
  }

  public function validationMinLen (string $input, int $min) : bool
  {
    if(mb_strlen($input) >= $min)
        return TRUE;

    $this->_last_error = lang('InputValidation','validation_MinLen',array('len'=>$min));
    return FALSE;
  }

  public function validationMaxLen (string $input, int $max) : bool
  {
		if(mb_strlen($input) <= $max)
		    return TRUE;

    $this->_last_error = lang('InputValidation','validation_MaxLen',array('len'=>$max));
		return FALSE;
  }

  public function validationExactLen (string $input, int $len) : bool
  {
    if(mb_strlen($input) == $max)
        return TRUE;

    $this->_last_error = lang('InputValidation','validation_ExactLen',array('len'=>$len));
    return FALSE;
  }

  public function validationValidEmail (string $input) : bool
  {
    if (!filter_var($input, FILTER_VALIDATE_EMAIL))
    {
      $this->_last_error = lang('InputValidation','validation_ValidEmail');
      return FALSE;
    }
    return TRUE;
  }

  public function validationAlnum(string $input) : bool
  {
    if (!ctype_alnum($input))
    {
      $this->_last_error = lang('InputValidation','validation_Alnum');
      return FALSE;
    }
    return TRUE;
  }

  public function validationAlpha(string $input) : bool
  {
    if (!ctype_alpha($input))
    {
      $this->_last_error = lang('InputValidation','validation_Alpha');
      return FALSE;
    }
    return TRUE;
  }

  public function validationControl(string $input) : bool
  {
    if (!ctype_cntrl($input))
    {
      $this->_last_error = lang('InputValidation','validation_Control');
      return FALSE;
    }
    return TRUE;
  }

  public function validationNumeric(string $input) : bool
  {
    if (!ctype_digit($input))
    {
      $this->_last_error = lang('InputValidation','validation_Numeric');
      return FALSE;
    }
    return TRUE;
  }

  public function validationGraph(string $input) : bool
  {
    if (!ctype_graph($input))
    {
      $this->_last_error = lang('InputValidation','validation_Graph');
      return FALSE;
    }
    return TRUE;
  }

  public function validationLower(string $input) : bool
  {
    if (!ctype_lower($input))
    {
      $this->_last_error = lang('InputValidation','validation_Lower');
      return FALSE;
    }
    return TRUE;
  }

  public function validationPrint(string $input) : bool
  {
    if (!ctype_print($input))
    {
      $this->_last_error = lang('InputValidation','validation_Print');
      return FALSE;
    }
    return TRUE;
  }

  public function validationPunct(string $input) : bool
  {
    if (!ctype_punct($input))
    {
      $this->_last_error = lang('InputValidation','validation_Punct');
      return FALSE;
    }
    return TRUE;
  }

  public function validationSpace(string $input) : bool
  {
    if (!ctype_space($input))
    {
      $this->_last_error = lang('InputValidation','validation_Space');
      return FALSE;
    }
    return TRUE;
  }

  public function validationUpper(string $input) : bool
  {
    if (!ctype_upper($input))
    {
      $this->_last_error = lang('InputValidation','validation_Upper');;
      return FALSE;
    }
    return TRUE;
  }

  public function validationXdigit(string $input) : bool
  {
    if (!ctype_xdigit($input))
    {
      $this->_last_error = lang('InputValidation','validation_Xdigit');
      return FALSE;
    }
    return TRUE;
  }

  public function validationAlphanumericSpaces(string $input) : bool
  {
    if (!preg_match('/^[A-Z0-9 ]+$/i', $input))
    {
      $this->_last_error = lang('InputValidation','validation_AlphanumericSpaces');
      return FALSE;
    }
    return TRUE;
  }

  public function validationAlphanumericDash(string $input) : bool
  {
    if (!preg_match('/^[a-z0-9_-]+$/i', $input))
    {
      $this->_last_error = lang('InputValidation','validation_AlphanumericDash');
      return FALSE;
    }
    return TRUE;
  }

  public function validationNumber(string $input) : bool
  {
    if (!preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $input))
    {
      $this->_last_error = lang('InputValidation','validation_Number');
      return FALSE;
    }
    return TRUE;
  }

  public function validationInteger(string $input) : bool
  {
    if (!preg_match('/^[\-+]?[0-9]+$/', $input))
    {
      $this->_last_error = lang('InputValidation','validation_Integer');
      return FALSE;
    }
    return TRUE;
  }

  public function validationGreaterThan(string $input, int $num) : bool
  {
    $p = is_numeric($input) ? $input + 0 : NULL;
    if ($p === NULL || $p <= $num)
    {
      $this->_last_error = lang('InputValidation','validation_GreaterThan', array('num'=>$num));
      return FALSE;
    }
    return TRUE;
  }

  public function validationGreaterThanEqualTo(string $input, int $num) : bool
  {
    $p = is_numeric($input) ? $input + 0 : NULL;
    if ($p === NULL || $p < $num)
    {
      $this->_last_error = lang('InputValidation','validation_GreaterThanEqualTo', array('num'=>$num));
      return FALSE;
    }
    return TRUE;
  }

  public function validationLessThan(string $input, int $num) : bool
  {
    $p = is_numeric($input) ? $input + 0 : NULL;
    if ($p === NULL || $p >= $num)
    {
      $this->_last_error = lang('InputValidation','validation_LessThan', array('num'=>$num));
      return FALSE;
    }
    return TRUE;
  }

  public function validationLessThanEqualTo(string $input, int $num) : bool
  {
    $p = is_numeric($input) ? $input + 0 : NULL;
    if ($p === NULL || $p > $num)
    {
      $this->_last_error = lang('InputValidation','validation_LessThanEqualTo', array('num'=>$num));
      return FALSE;
    }
    return TRUE;
  }

  public function validationInList(string $input, array $list) : bool
  {
    if (!in_array($input, $list))
    {
      $this->_last_error = lang('InputValidation','validation_InList');
      return FALSE;
    }
    return TRUE;
  }

  public function validationValidBase64(string $input) : bool
  {
    if (base64_encode(base64_decode($input)) !== $input)
    {
      $this->_last_error = lang('InputValidation','validation_Base64');
      return FALSE;
    }
    return TRUE;
  }

  public function validationValidName (string $input) : bool
  {
    if(preg_match("/^([a-zÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñðòóôõöùúûüýÿ '-])+$/i", $input))
      return TRUE;

    $this->_last_error = lang('InputValidation','validation_ValidName');
    return FALSE;
  }

  public function validationValidPhoneNumber($value)
  {
    if(!preg_match("/^[+0-9][0-9()]+$/", $input))
    {
      $this->_last_error = lang('InputValidation','validation_ValidPhoneNumber');
      return FALSE;
    }
    return TRUE;
  }

  public function validationUniqueInTable($value, string $table, string $column)
  {
    $count = \core\Model::db($table)->where($column,'=',$value)->count();
    if ($count > 0)
    {
      $this->_last_error = lang('InputValidation','validation_UniqueInTable');
      return FALSE;
    }
    return TRUE;
  }

}
