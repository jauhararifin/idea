-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2016 at 01:49 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `idea`
--

-- --------------------------------------------------------

--
-- Table structure for table `id_config`
--

CREATE TABLE `id_config` (
  `id` int(11) NOT NULL,
  `key` varchar(256) NOT NULL,
  `type` varchar(256) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_config`
--

INSERT INTO `id_config` (`id`, `key`, `type`, `value`) VALUES
(1, 'siteName', 'string', 'Idea'),
(3, 'view.theme', 'string', 'bootstrap'),
(7, 'post.posts_per_page', 'int', '5');

-- --------------------------------------------------------

--
-- Table structure for table `id_member`
--

CREATE TABLE `id_member` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `registered` datetime NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_member`
--

INSERT INTO `id_member` (`id`, `username`, `fullname`, `email`, `password`, `registered`, `data`) VALUES
(1, 'jauhararifin', 'Jauhar Arifin', 'jauhararifin10@gmail.com', '$2y$10$W.2C1REJUevRTQ6vL2U55eYooOgB.DX2zoIj0iKnr.nR/l0h0XBAe', '2016-03-09 07:21:19', ''),
(3, 'jauhar', 'Jauhar Arifin', 'jauhararifin11@gmail.com', '$2y$10$Z/WrTFrkTInsPtDgtAsX6e2WqHOpC8TEFZwC8Q/G7KAkacZ32Jy/O', '2016-03-09 12:17:24', ''),
(4, 'coba1', 'Coba 1', 'coba1@coba.coba', '$2y$10$2Li5cd.wJqrywUS0hkGc9.4H2f87gfSs3kf2C2xUFAYpsYWnHVfke', '2016-03-09 12:22:11', ''),
(5, 'coba2', 'Coba 2', 'coba2@coba.coba', '$2y$10$zZsJnBVjdBZwxvJ0uU3Phu4ynK69qRViyKI8BXir48urYxp/mTu0a', '2016-03-09 13:12:22', '');

-- --------------------------------------------------------

--
-- Table structure for table `id_post`
--

CREATE TABLE `id_post` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `made` datetime NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_post`
--

INSERT INTO `id_post` (`id`, `author_id`, `title`, `content`, `made`, `data`) VALUES
(1, 1, 'Lorem Ipsum', '<p>Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>', '2016-03-09 07:23:15', ''),
(2, 1, 'Mengapa Kita Menggunakannya?', '<p>Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh isi tulisan dari sebuah halaman saat ia melihat tata letaknya. Maksud penggunaan Lorem Ipsum adalah karena ia kurang lebih memiliki penyebaran huruf yang normal, ketimbang menggunakan kalimat seperti "Bagian isi disini, bagian isi disini", sehingga ia seolah menjadi naskah Inggris yang bisa dibaca. Banyak paket Desktop Publishing dan editor situs web yang kini menggunakan Lorem Ipsum sebagai contoh teks. Karenanya pencarian terhadap kalimat "Lorem Ipsum" akan berujung pada banyak situs web yang masih dalam tahap pengembangan. Berbagai versi juga telah berubah dari tahun ke tahun, kadang karena tidak sengaja, kadang karena disengaja (misalnya karena dimasukkan unsur humor atau semacamnya)</p>', '2016-03-09 07:23:50', ''),
(3, 1, 'Dari Mana Asalnya?', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\r\n\r\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2016-03-09 09:06:14', ''),
(4, 1, 'Dari Mana Mendapatkannya?', '<p>Ada banyak variasi tulisan Lorem Ipsum yang tersedia, tapi kebanyakan sudah mengalami perubahan bentuk, entah karena unsur humor atau kalimat yang diacak hingga nampak sangat tidak masuk akal. Jika anda ingin menggunakan tulisan Lorem Ipsum, anda harus yakin tidak ada bagian yang memalukan yang tersembunyi di tengah naskah tersebut. Semua generator Lorem Ipsum di internet cenderung untuk mengulang bagian-bagian tertentu. Karena itu inilah generator pertama yang sebenarnya di internet. Ia menggunakan kamus perbendaharaan yang terdiri dari 200 kata Latin, yang digabung dengan banyak contoh struktur kalimat untuk menghasilkan Lorem Ipsun yang nampak masuk akal. Karena itu Lorem Ipsun yang dihasilkan akan selalu bebas dari pengulangan, unsur humor yang sengaja dimasukkan, kata yang tidak sesuai dengan karakteristiknya dan lain sebagainya.</p>', '2016-03-09 09:07:00', ''),
(5, 1, 'Coba 1', 'cobain ngisi cerita nih', '2016-03-09 13:11:37', ''),
(6, 5, 'What Is Lorem Ipsum?', '<p><strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '2016-03-09 13:14:23', ''),
(7, 5, 'Where Can I Get One?', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id semper nunc. Aenean finibus felis ac turpis venenatis maximus. Integer ut nibh aliquam, egestas orci et, ultrices ligula. Morbi sagittis suscipit velit ac gravida. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus in ligula non enim fermentum fringilla nec vitae tortor. Mauris consequat nisi vitae elit mattis malesuada. Donec at nulla ut purus consequat bibendum. Sed ultrices tempor lacus, non egestas nunc finibus vel. Aliquam in lectus a risus lacinia lacinia eget vel urna.</p><p>Nullam accumsan lacinia orci, non aliquam ipsum iaculis nec. Morbi ut nunc posuere, efficitur turpis in, vehicula eros. Duis imperdiet dui quam, eget laoreet lorem tristique ac. Nam vitae tristique odio. Phasellus posuere nibh in metus elementum, sed vehicula nulla dapibus. Suspendisse potenti. Vestibulum sit amet mollis justo. Nullam vestibulum finibus magna non auctor. Phasellus posuere dignissim justo, vitae bibendum odio varius nec. Cras nulla dui, semper sed urna in, facilisis auctor nisl. Maecenas hendrerit fermentum porttitor. Sed rhoncus in leo quis vehicula. Sed suscipit enim quis scelerisque scelerisque. Nam placerat, arcu eu varius commodo, nisi augue suscipit ex, eget pulvinar est erat pulvinar mi. Suspendisse cursus eleifend felis, sed aliquet ex rhoncus sed. Mauris semper lorem id sem consequat ultricies.</p><p>Vivamus ante ex, volutpat faucibus fermentum eu, porta non sapien. Vivamus vel condimentum orci. Maecenas vitae augue vulputate, auctor velit et, commodo risus. Suspendisse a tortor suscipit, blandit risus vel, sagittis erat. Duis egestas velit nunc, ut bibendum dolor euismod vel. Proin vitae ipsum sit amet ante maximus ornare ut sed est. Cras a mauris in purus interdum ultricies ac sit amet purus. Morbi in nisi dolor. Duis ac interdum massa, quis venenatis neque. Quisque interdum consectetur ullamcorper. Sed metus leo, maximus vel vestibulum et, pretium ut mi. Ut cursus, dolor eu porttitor interdum, libero nulla pharetra lectus, non ultricies lectus massa vitae orci. Aliquam eget ante id tellus dignissim convallis. Quisque non augue sed quam finibus venenatis. Donec dignissim laoreet venenatis. Nullam tincidunt neque eget dignissim cursus.</p>', '2016-03-09 13:24:17', ''),
(8, 3, 'Mengapa Kita Menggunakannya?', '<p>Sudah merupakan fakta bahwa seorang pembaca akan terpengaruh oleh isi tulisan dari sebuah halaman saat ia melihat tata letaknya. Maksud penggunaan Lorem Ipsum adalah karena ia kurang lebih memiliki penyebaran huruf yang normal, ketimbang menggunakan kalimat seperti "Bagian isi disini, bagian isi disini", sehingga ia seolah menjadi naskah Inggris yang bisa dibaca. Banyak paket Desktop Publishing dan editor situs web yang kini menggunakan Lorem Ipsum sebagai contoh teks. Karenanya pencarian terhadap kalimat "Lorem Ipsum" akan berujung pada banyak situs web yang masih dalam tahap pengembangan. Berbagai versi juga telah berubah dari tahun ke tahun, kadang karena tidak sengaja, kadang karena disengaja (misalnya karena dimasukkan unsur humor atau semacamnya)</p>', '2016-03-09 07:23:50', ''),
(9, 3, 'Dari Mana Mendapatkannya?', '<p>Ada banyak variasi tulisan Lorem Ipsum yang tersedia, tapi kebanyakan sudah mengalami perubahan bentuk, entah karena unsur humor atau kalimat yang diacak hingga nampak sangat tidak masuk akal. Jika anda ingin menggunakan tulisan Lorem Ipsum, anda harus yakin tidak ada bagian yang memalukan yang tersembunyi di tengah naskah tersebut. Semua generator Lorem Ipsum di internet cenderung untuk mengulang bagian-bagian tertentu. Karena itu inilah generator pertama yang sebenarnya di internet. Ia menggunakan kamus perbendaharaan yang terdiri dari 200 kata Latin, yang digabung dengan banyak contoh struktur kalimat untuk menghasilkan Lorem Ipsun yang nampak masuk akal. Karena itu Lorem Ipsun yang dihasilkan akan selalu bebas dari pengulangan, unsur humor yang sengaja dimasukkan, kata yang tidak sesuai dengan karakteristiknya dan lain sebagainya.</p>', '2016-03-09 14:07:00', ''),
(10, 3, 'Dari Mana Asalnya?', '<p>Tidak seperti anggapan banyak orang, Lorem Ipsum bukanlah teks-teks yang diacak. Ia berakar dari sebuah naskah sastra latin klasik dari era 45 sebelum masehi, hingga bisa dipastikan usianya telah mencapai lebih dari 2000 tahun. Richard McClintock, seorang professor Bahasa Latin dari Hampden-Sidney College di Virginia, mencoba mencari makna salah satu kata latin yang dianggap paling tidak jelas, yakni consectetur, yang diambil dari salah satu bagian Lorem Ipsum. Setelah ia mencari maknanya di di literatur klasik, ia mendapatkan sebuah sumber yang tidak bisa diragukan. Lorem Ipsum berasal dari bagian 1.10.32 dan 1.10.33 dari naskah "de Finibus Bonorum et Malorum" (Sisi Ekstrim dari Kebaikan dan Kejahatan) karya Cicero, yang ditulis pada tahun 45 sebelum masehi. BUku ini adalah risalah dari teori etika yang sangat terkenal pada masa Renaissance. Baris pertama dari Lorem Ipsum, "Lorem ipsum dolor sit amet..", berasal dari sebuah baris di bagian 1.10.32.</p>\r\n\r\n<p>Bagian standar dari teks Lorem Ipsum yang digunakan sejak tahun 1500an kini di reproduksi kembali di bawah ini untuk mereka yang tertarik. Bagian 1.10.32 dan 1.10.33 dari "de Finibus Bonorum et Malorum" karya Cicero juga di reproduksi persis seperti bentuk aslinya, diikuti oleh versi bahasa Inggris yang berasal dari terjemahan tahun 1914 oleh H. Rackham.</p>', '2016-03-09 08:06:14', ''),
(11, 3, 'Coba 2', 'cobain ngisi cerita nih', '2016-03-09 13:11:37', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `id_config`
--
ALTER TABLE `id_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `id_member`
--
ALTER TABLE `id_member`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fullname` (`fullname`);

--
-- Indexes for table `id_post`
--
ALTER TABLE `id_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `title` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `id_config`
--
ALTER TABLE `id_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `id_member`
--
ALTER TABLE `id_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `id_post`
--
ALTER TABLE `id_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
